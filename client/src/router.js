import Vue from "vue";
import Router from "vue-router";
import App from "./App";
import AppHeader from "./layout/AppHeader";
import AppFooter from "./layout/AppFooter";
import Components from "./views/Components.vue";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Profile from "./views/Profile.vue";
import Home from "./views/Home.vue";
import Booking from "./views/Booking.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        header: AppHeader,
        default: App,
        footer: AppFooter
      },
      children: [
        {
          path: "",
          name: "nurses",
          component: Home
        },
        {
          path: "booking",
          name: "booking",
          component: Booking
        },
        {
          path: "login",
          name: "login",
          component: Login
        },
        {
          path: "profile",
          name: "profile",
          component: Profile
        },
        {
          path: "register",
          name: "register",
          component: Register
        }
      ]
    },
    {
      path: "/components",
      name: "components",
      components: {
        header: AppHeader,
        default: Components,
        footer: AppFooter
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
