import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { sync } from "vuex-router-sync";
import Argon from "./plugins/argon-kit";
import axios from "axios";
import VueAxios from "vue-axios";

sync(store, router);

Vue.config.productionTip = false;
Vue.use(Argon);
Vue.use(VueAxios, axios);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
