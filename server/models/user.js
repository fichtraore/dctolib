const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    name :{
        type: String,
        required: [true, 'Le champ nom est requis']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'Le champ email est requis'],
        validate: {
            validator: function(email) {
              return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,24})?$/.test(email);
            },
            message: props => `${props.value} n'est pas une adresse email valide`
        },
    },
    role: {
        type: String,
        default: 'client'
    },
    password: {
        type: String,
        required: [true, 'Le mot de passe est requis'],
        minLenght: [6, 'Le mot de passe doit contenir au moins 6 caract\ères']
    },
    salt: {
        type: String,
        select: false,
    },
    availabilities: [{ type: mongoose.Schema.Types.ObjectId, ref: "Availability" }],
    bookings: [{ type: mongoose.Schema.Types.ObjectId, ref: "Booking" }],
})

const User = mongoose.model('User', userSchema)

module.exports = User