const mongoose = require('mongoose')

const bookingSchema = mongoose.Schema({
    start :{
        type: Date
    },
    end: {
        type: Date
    },
    availability: { type: mongoose.Schema.Types.ObjectId, ref: "Availability", required: true  },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true  },
})

const Booking = mongoose.model('Booking', bookingSchema)

module.exports = Booking