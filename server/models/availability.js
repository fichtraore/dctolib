const mongoose = require('mongoose')

const availabilitySchema = mongoose.Schema({
    start :{
        type: Date,
    },
    end: {
        type: Date
    },
    booked: { type: Boolean, default: false },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
})

const Availability = mongoose.model('Availability', availabilitySchema)

module.exports = Availability