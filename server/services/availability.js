const Availability =  require('../models/availability')

class availabilityService {
    async find(query) {
        return await Availability.find(query).sort({"start": 1})
    }

    async store(availability) {
        // Create and return a availability record to the client
        return await Availability.create(availability)
    }

    async storeMany(availabilitys) {
        // Create and return Availabilitys records to the client
        return await Availability.insertMany(availabilitys)
    } 

    async update(availability){
        // set Availability updated records
        return await Availability.findByIdAndUpdate(availability._id, availability, { new: true })
    }

    async delete(id) {
        const record = await Availability.findById(id)
        await record.deleteOne()
        return 'OK'
    } 
}

module.exports = availabilityService