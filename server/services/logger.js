const { createLogger, transports, format } = require('winston')

const logger = createLogger({
  // Define log message format
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  // where and how to register logs files
  transports: [
    new transports.File({
      filename: './logs/all.log',
      json: false,
      maxsize: 5242880,
      maxFiles: 10,
    }),
    new transports.Console(),
  ]
})

module.exports = logger