const argon2 = require('argon2')
const { randomBytes } = require('crypto')
const User =  require('../models/user')

class UserService {
    async find(query) {
        return await User.find(query)
    }

    async store(user) {
        // Create and return a user record to the client
        const salt = randomBytes(32)
        const randomPassword = randomBytes(10).toString('hex')
        const passwordHashed = await argon2.hash(user.password || randomPassword, { salt })

        let userRecord = new User({ 
            name: user.name,
            email: user.email,
            password: passwordHashed,
            salt: salt.toString('hex'),
            role: user.role, 
        })
        return await userRecord.save()
        .then((result) => {
            return this.resource(userRecord)
        }).catch(async(err) => {
            throw new Error(err)
        })
    }

    async storeMany(users) {
        const records = await Promise.all(
            users.map(async user => {
                const salt = randomBytes(32)
                const randomPassword = randomBytes(10).toString('hex')
                user.password = await argon2.hash(user.password || randomPassword, { salt })
                user.salt = salt.toString('hex')
                return user
            })
        )
        // Create and return users records to the client
        const userRecords = await User.insertMany(records)
        return userRecords
    } 

    async update(user){
        // set user updated records
        return await User.findByIdAndUpdate(user._id, user, { new: true })
    }

    async delete(id) {
        const userRecord = await User.findById(id)
        await userRecord.deleteOne()
        return 'OK'
    } 
    
    async signup(user) {
        // A salt is random data that is used as an additional input to the hashing function, 
        // The salt is randomly generated for every new user record.
        const salt = randomBytes(32);
        const passwordHashed = await argon2.hash(user.password, { salt })

        const userRecord =  await User.create({
            name: user.name,
            email: user.email,
            password: passwordHashed,
            salt: salt.toString('hex')
        })

        // Make sure to nerver return the password
        return { email: userRecord.email, name: userRecord.name }
    }

    resource(user) {
        return {
            _id: user._id, 
            name: user.name, 
            email: user.email,
            role: user.role
        }
    }
}

module.exports = UserService