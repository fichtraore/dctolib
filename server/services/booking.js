const Booking =  require('../models/booking')
const Availability = require('../models/availability')

class bookingService {
    async find(query) {
        return await Booking.find(query)
    }

    async store(booking) {
        // Meeting duration in milliseconds
        const meetingDuration = parseInt(booking.duration) * 60 * 1000
        // Create and return a source record to the client
        const availability = await Availability.findById(booking.availability)
        
        // Get the availability duration
        const difference = availability.end.getTime() - availability.start.getTime()
        // Check if the booking time is less than the availability
        // Add a new booking then
        // @duration in minutes
        let newAvailability = null
        if(meetingDuration < difference) {
            newAvailability = await Availability.create({ 
                start: availability.start.getTime() + meetingDuration,
                end: availability.end,
                user: availability.user
            })
        }
        // Save records
        availability.booked = true
        availability.end = availability.start.getTime() + meetingDuration
        await availability.save()
        console.log(availability, newAvailability)
        const bookingRecord = await Booking.create(booking)
        return {
            booking: bookingRecord,
            availability: availability,
            newAvailability: newAvailability
        }
    }

    async storeMany(bookings) {
        // Create and return Bookings records to the client
        return await Booking.insertMany(bookings)
    } 

    async update(booking){
        // set Booking updated records
        return await Booking.findByIdAndUpdate(booking._id, booking, { new: true })
    }

    async delete(id) {
        const record = await Booking.findById(id)
        await record.deleteOne()
        return 'OK'
    } 
}

module.exports = bookingService