const argon2 = require('argon2')
const jsonwebtoken = require('jsonwebtoken')
const { jwt } = require('../config')
const User =  require('../models/user')

class AuthService {
    async login(email, password) {
        const userRecord = await User.findOne({ email: email })
        if(!userRecord) {
            throw new Error('Username or password not found')
        }
        else {
            // Check hashed password validity with argon2
            const correctPassword = await argon2.verify(userRecord.password, password)
            if(!correctPassword) {
                throw new Error('Username or password not found')
            }
        }
        return {
            user: userRecord,
            token: this.generateAuthToken(userRecord)
        }
    }

    // Generate a JWT from the user data
    generateAuthToken(user) {
        const data=  {
            _id: user.id,
            name: user.name,
            email: user.email
        }

        return jsonwebtoken.sign({ data}, jwt.secret, {expiresIn: jwt.expire})
    }
}

module.exports = AuthService