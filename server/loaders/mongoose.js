const mongoose = require('mongoose')
const { mongo } = require('../config')
const logger = require('../services/logger')
const databaseSeeder = require('../seeders')

// Connect to MongoDB daemon
module.exports = async () => {
    mongoose.connect(mongo.uri, { 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
    })
    .then(async () => {
        logger.info('MongoDB connected')
        // Execute databse seeders
        await databaseSeeder()
    })
    .catch(error => logger.error(error))
} 