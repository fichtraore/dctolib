const { api, parse, mongo } = require('../config')
const routes = require('../routes')
const ParseServer = require('parse-server').ParseServer

const parseServer = new ParseServer({
    appId: parse.id,
    masterKey: parse.key,
    serverURL: parse.uri,
    databaseURI: mongo.uri, // Connection string for your MongoDB database
    cloud: "./cloud/main",
});
module.exports = async ({ app }) => {
    
    // Set express routes
    // Parse server routes are accessible on /api/parse
    routes.use('/parse', parseServer)
    app.use(api.prefix, routes)

    return app
}