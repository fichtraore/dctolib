const parseLoader = require('./parse')
const mongooseLoader = require('./mongoose')

module.exports = async ({ expressApp }) => {
    
    // Initialize mongoDB
    await mongooseLoader()

    // Initialize express and parse server
    await parseLoader({ app: expressApp })
}