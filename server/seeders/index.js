const { mongo } = require('../config')
const userSeeder = require('./user')

module.exports = async () => {
    
    if(mongo.seed == 'yes') {
        await userSeeder()
    }
}