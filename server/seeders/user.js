
const User = require('../models/user')
const Availability = require('../models/availability')
const UserService = require('../services/user')
const Booking = require('../models/booking')


const userService = new UserService()


module.exports = async () => {
    await User.deleteMany()
    await Availability.deleteMany()
    await Booking.deleteMany()

    const users = await userService.storeMany([
        {
            name: 'Bepo Heart',
            email: 'bepo@heart.com',
            password: 'secret',
            role: 'nurse',
        },
        {
            name: 'Satchi Heart',
            email: 'satchi@heart.com',
            password: 'secret',
            role: 'nurse',
        },
        {
            name: 'Penguin Heart',
            email: 'penguin@heart.com',
            password: 'secret',
            role: 'nurse',
        },
        {
            name: 'Client Demo',
            email: 'client@elaafy.com',
            password: 'secret',
            role: 'client'
        }
    ])
    const dates = []
    let loopDay = new Date(new Date().setHours(8, 0, 0));
    for (let i = 0; i <= 30; i++) {
    loopDay.setDate(loopDay.getDate() + 1);
        dates.push(new Date(loopDay))
    }

    users.filter(x => x.role === 'nurse')
        .forEach(async(user) => {
            for (const date of dates) {
                for (let i = 0; i < 20; i++) {
                    const _date = new Date(date).getTime()
                    const millis = 30 * 60 * 1000
                    await Availability.create({
                        user: user._id,
                        start: _date + i * millis,
                        end: _date + (i + 1) * millis,
                    })        
                }
            }
        })
}