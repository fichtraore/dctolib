/**
 * @param str
 */
const slugify = (str, separator = '-') => {
    if(!str) return ''
    str = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    str = str.replace(/^\s+|\s+$/g, ""); // trim
    str = str.toLowerCase();
    
    str = str
      .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
      .replace(/\s+/g, "-") // collapse whitespace and replace by -
      .replace(/-+/g, "-") // collapse dashes
      .replace(/^-+/, "") // trim - from start of text
      .replace(/-+$/, "") // trim - from end of text
      .replace(/-/g, separator)
  
    return str
}

module.exports = slugify