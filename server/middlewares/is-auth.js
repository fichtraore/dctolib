const jwt = require('express-jwt')
const { jwt: config }  = require('../config')

// We are assuming that the JWT will come in the header Authorization.
const getTokenFromHeader = (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1]
    } 
    else if (req.query && req.query.token) {
        return req.query.token;
    }
    return null;
}

module.exports = jwt({
    secret: config.secret, // Has to be the same that we used to sign the JWT

    algorithms: ['HS256'],
    
    userProperty: 'token', // this is where the next middleware can find the encoded data generated in services/auth:generateAuthToken -> 'req.token'

    getToken: getTokenFromHeader, // A function to get the auth token from the request
})