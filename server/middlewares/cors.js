const cors = require('cors')
const { api } = require('../config')

const corsOptions = {
    origin: function (origin, callback) {
        console.log(origin)
        if (api.cors.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}

module.exports = cors(corsOptions)