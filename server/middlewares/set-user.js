const User = require('../models/user')
const url = require('url');
const querystring = require('querystring');

let rawUrl = 'https://stackabuse.com/?page=2&limit=3';

let parsedUrl = url.parse(rawUrl);
let parsedQs = querystring.parse(parsedUrl.query);

module.exports = async (req, res, next) => {
    if(!req.token) {
        return res.status(401).end('Token non trouvé')
    }
    // Load user and role 
    const decodedTokenData = req.token.data
    const userRecord = await User.findOne({ _id: decodedTokenData._id }, 'name email role')
   
    if(!userRecord) {
        return res.status(401).end('Utilisateur non trouvé')
    }
    req.user = userRecord
        
    return next()
}
   