module.exports = (role) => {
    return  async (req, res, next) => {
        // Check user role
        if(req.user && req.user.role === role) {
            return next()
        }
        return res.status(403).end('Accès non autorisé')
    }
}