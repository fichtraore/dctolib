if(process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}
// Export application configuration variables
module.exports = {
    env: process.env.NODE_ENV || 'production',
    port: process.env.PORT || 1337,
    host: process.env.HOST || 'localhost',
    baseUrl: process.env.BASE_URL || 'http://localhost',
    api: {
        prefix:  process.env.API_PREFIX || '/api',
    },
    parse: {
        id:  process.env.PARSE_SERVER_ID || 'QChMm70F0d4pvJKlP9BFgRwLsLO76JnGXJSXh3NB',
        key:  process.env.PARSE_SERVER_KEY || '0wwAaIpnpX3s7AeTAjvttzuevQGXTWCOKkZY7civ',
        uri:  process.env.PARSE_SERVER_URI || 'http://localhost:1337/api',
    },
    mongo: {
        uri: process.env.MONGO_URI || 'mongodb://localhost:27017/parse',
        seed: 'yes'
    },

    jwt: {
        secret: process.env.JWT_SECRET ||'my jwt secret',
        expire: process.env.JWT_EXPIRE || '24h',
    },
}
