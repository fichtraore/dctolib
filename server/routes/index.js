const express = require('express')
const cors = require('cors')
const isAuth = require('../middlewares/is-auth')
const setUser = require('../middlewares/set-user')
const hasRole = require('../middlewares/has-role')
const httpLogger = require('../middlewares/logger')

const app = express()

// import application routes
const auth = require('./auth')
const user = require('./user')
const availability = require('./availability')
const booking = require('./booking')

// Add global middlewares
app.use(cors())
app.use(httpLogger)

// Add authentification routes
auth(app)

// Add application access middlewares
// app.use(isAuth)
// app.use(setUser)
// app.use(hasRole)

// Add resources routes
app.use('/users', user)
app.use('/bookings', booking)
app.use('/availabilities', availability)

module.exports = app