const { Router } = require('express')
const UserService = require('../services/user')

const router = new Router()
const User = new UserService()

router.get('/', async(request, response) => {
    const query = request.query
    try {
        const users = await User.find(query)
        return response.send(users)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.post('/store', async(request, response) => {
    const user = request.body
    try {
        const userRecord = await User.store(user)
        return response.send(userRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.put('/update', async(request, response) => {
    const user = request.body.user
    try {
        const userRecord = await User.update(user)
        return response.send(userRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.put('/remove/:userId', async(request, response) => {
    try {
        const userRecord = await User.removeOrRestore(request.params.userId, new Date())
        return response.send(userRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.put('/restore/:userId', async(request, response) => {
    try {
        const userRecord = await User.removeOrRestore(request.params.userId)
        return response.send(userRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.delete('/delete/:userId', async(request, response) => {
    try {
        const userRecord = await User.delete(request.params.userId)
        return response.send(userRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

module.exports = router