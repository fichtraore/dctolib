const AuthService = require('../services/auth')
const isAuth = require('../middlewares/is-auth')
const setUser = require('../middlewares/set-user')

module.exports = (app) => {

    app.post('/login', async(request, response) => {
        const email = request.body.user.email
        const password = request.body.user.password
        try {
            const Auth = new AuthService()
            const { user, token } = await Auth.login(email, password)
            return response.status(200).json({ user, token }).end()
        }
        catch(error) {
            console.log(error.message)
            return response.status(401).json(error.message).end()
        }
    })
    
    app.get('/auth/user', isAuth, setUser, async(request, response) => {
        // Return current user informations 
        return response.json({ user: request.user }).status(200).end()
    })

    app.post('/logout', isAuth, async(request, response) => {
        // Return ok after clearing user data in localstorage and browser cookies
        return response.json('ok').status(200).end()
    })
}
