const { Router } = require('express')
const AvailabilityService = require('../services/availability')

const router = new Router()
const Availability = new AvailabilityService()

router.get('/', async(request, response) => {
    const query = request.query
    try {
        const availabilities = await Availability.find(query)
        return response.send(availabilities)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.post('/store', async(request, response) => {
    const availability = request.body.availability
    try {
        const availabilityRecord = await Availability.store(availability)
        return response.send(availabilityRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.put('/update', async(request, response) => {
    const availability = request.body.availability
    try {
        const availabilityRecord = await Availability.update(availability)
        return response.send(availabilityRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.delete('/delete/:availabilityId', async(request, response) => {
    try {
        const availabilityRecord = await Availability.delete(request.params.availabilityId)
        return response.send(availabilityRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

module.exports = router