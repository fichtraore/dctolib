const { Router } = require('express')
const BookingService = require('../services/booking')

const router = new Router()
const Booking = new BookingService()

router.get('/', async(request, response) => {
    const query = request.query
    try {
        const bookings = await Booking.find(query)
        return response.send(bookings)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.post('/store', async(request, response) => {
    const booking = request.body.booking
    try {
        const bookingRecord = await Booking.store(booking)
        return response.send(bookingRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.put('/update', async(request, response) => {
    const booking = request.body.booking
    try {
        const bookingRecord = await Booking.update(booking)
        return response.send(bookingRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

router.delete('/delete/:bookingId', async(request, response) => {
    try {
        const bookingRecord = await Booking.delete(request.params.bookingId)
        return response.send(bookingRecord)
    } catch (error) {
        console.log(error)
        return response.status(520).json(error.message).end()
    }
})

module.exports = router