const express = require('express')
const logger  = require('./services/logger')
const loaders = require('./loaders')

async function start () {

    const app = express()

    // Requests body json parser 
    app.use(express.json({ limit: '5mb' }))
    
    // Load api modules
    await loaders({ expressApp: app })

    // Configuring port
    const port = process.env.PORT || 1337

    // Listening to port
    app.listen(port)
    logger.info(`Listening On http://localhost:${port}/api`)

}

start()
