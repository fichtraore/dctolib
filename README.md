# Requirements
- [Node.js](https://nodejs.org/) >= 6.x
- [MongoDB](https://www.mongodb.com/try/download/community) >= 4
<br />

# Server

## Mongo db
You need a running mongodb daemon
mongo-runner only for test purpose or install mongo from the provider
```
 npm install -g  mongodb-runner
 mongodb-runner start
```
The default mongo db uri
MONGO_URI = mongodb://localhost:27017/parse

## Scripts

**Install Modules**
```bash
$ npm install
```
**Run**
* `npm run serve` to start the app. Visit [localhost:1337/api](http://localhost:1337/api) for the server


# Client

## Scripts

**Install Modules**
```bash
$ npm install
```

**Run**
* `npm run start` to start the app. Visit [localhost:8080](http://localhost:8080) in your browser. The `default port 8080` can be overwritten by updating the `package.json`, line with `serve` attribute: `vue-cli-service serve --port 8080`

**Production Build**
* `npm run build` - build the app in `dist` directory

**Test the production build**
`serve -s dist` - and visit [localhost:8080](http://localhost:8080) in your browser. 

<br />